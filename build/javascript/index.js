'use strict';

window.onload = function () {
    var image = document.getElementById('image');
    var offsetX = void 0;
    var offsetY = void 0;
    var x = void 0;
    var y = void 0;
    image.onmousemove = function zoom(e) {
        var zoomer = e.currentTarget;
        e.offsetX ? offsetX = e.offsetX : offsetX = e.touches[0].pageX;
        e.offsetY ? offsetY = e.offsetY : offsetX = e.touches[0].pageX;
        x = offsetX / zoomer.offsetWidth * 100;
        y = offsetY / zoomer.offsetHeight * 100;
        zoomer.style.backgroundPosition = x + '% ' + y + '%';
    };
};